from django.conf.urls import url

from rest_framework.authtoken import views as rest_views

from .routers import HybridRouter


r = HybridRouter()
r.root_docs = """
Root API endpoints.
"""

r.view_urls = [
    url(r'^token-auth/?$', rest_views.obtain_auth_token, name='token-auth'),
]

urlpatterns = r.urls