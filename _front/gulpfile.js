const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync');
const del = require('del');
const runSequence = require('run-sequence');
const replace = require('gulp-replace');
const wiredep = require('wiredep').stream;

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const jsSrc = 'app/js/**/*.js';
const imgSrc = 'app/images/**/*';
const htmlSrc = 'app/templates/**/*.html';
const scssSrc = 'app/styles/**/*.scss';

const staticSrc = '../build/_static/';
const templateSrc = '../build/templates/';

gulp.task('styles', () => {
  return gulp.src(scssSrc)
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('app/css'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src(jsSrc)
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(staticSrc + 'js'))
    .pipe(reload({stream: true}));
});

gulp.task('move_templates', () => {
  return gulp.src(htmlSrc)
    .pipe($.useref({searchPath: ['app']}))

    .pipe(replace('script src="','script src="{% static "'))
    .pipe(replace('.js"','.js" %}"'))
    .pipe(replace('href="css','href="{% static "css'))
    .pipe(replace('.css"','.css" %}"'))
    
    .pipe($.if('*.js', gulp.dest(staticSrc)))
    .pipe($.if('*.css', gulp.dest(staticSrc)))
    .pipe($.if('*.html', gulp.dest(templateSrc)))
});

function lint(files, options) {
  return gulp.src(files)
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint(options))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint(jsSrc, {
    fix: true
  })
    .pipe(gulp.dest('app/js'));
});

gulp.task('html', ['styles', 'scripts'], () => {
  return gulp.src([
    'app/**/*.html',
    '../build/_static/**/*.css'
    ])
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if('*.html', $.htmlmin({
      collapseWhitespace: true, 
      ignoreCustomFragments: [ (/\{\%[^\%]*?\%\}/g) ]
      })))
    
    .pipe($.if('*.js', gulp.dest(staticSrc)))
    .pipe($.if('*.css', gulp.dest(staticSrc)))
    .pipe($.if('*.html', gulp.dest('../build/')))
});

gulp.task('images', () => {
  return gulp.src(imgSrc)
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{cleanupIDs: false}]
    })))
    .pipe(gulp.dest(staticSrc + 'images'));
});

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('app/fonts/**/*'))
    .pipe(gulp.dest(staticSrc + 'fonts'));
});

gulp.task('clean', del.bind(null, ['../build/'], {force: true}));
gulp.task('serve', ['styles', 'scripts', 'move_templates', 'fonts', 'images'], () => {
  
  gulp.watch(scssSrc, () => runSequence('styles','move_templates', reload));
  gulp.watch(jsSrc, () => runSequence('scripts', reload));
  gulp.watch(imgSrc, () => runSequence('images'));
  gulp.watch('app/fonts/**/*', () => runSequence('fonts', reload));
  gulp.watch('bower.json', () => runSequence('wiredep', 'fonts', reload));
  gulp.watch(htmlSrc, () => runSequence('move_templates', reload));

  browserSync({
    notify: false,
    proxy: 'localhost:8000',
    port: 9000,
  });
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src(scssSrc)
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('app/css'));

  gulp.src('app/*.html')
    .pipe(wiredep({
      exclude: ['bootstrap-sass'],
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('build', ['lint', 'html', 'images', 'fonts'], () => {
  return gulp.src('../build/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], () => {
  gulp.start('build');
});
