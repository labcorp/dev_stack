from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.utils.translation import gettext_lazy as _

from conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^', include('apps.core.urls', namespace='core')),
    
    url(r'^api/v1/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include('apps.api.urls', namespace='api')),
    url(r'^api/?$', RedirectView.as_view(pattern_name='api:v1:api-root')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
